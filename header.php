<!DOCTYPE html>
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<html>
    <head>
        <!--=== META TAGS ===-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   
        <!--=== LINK TAGS ===-->
        <link rel="shortcut icon" href="<?php echo THEME_DIR; ?>/images/favicon.ico" />
        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS2 Feed" href="<?php bloginfo('rss2_url'); ?>" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        
        <!--=== TITLE ===-->  
        <title><?php wp_title( '|', true, 'right' );bloginfo( 'name' ); ?></title>
     
        <!--=== WP_HEAD() ===-->
        <?php wp_head(); ?>
        
    </head>
    <body>