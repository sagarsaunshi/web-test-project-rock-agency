# README #

Word Press Setup
1. Theme : Rock Agency
2. Plugin :Advance custom field 

1. Theme : Rock Agency
The theme has been implemented according to the given requirements. Functionalities implemented are follows:

1.	Git Version Control (Code reposited at Bit Bucket using command line Git bash)
2.	Build System (Gulp has been implemented as a task runner and various packages to make coding easier )
3.	Logo ( Svg Image  for logo)
4.	Preprocessor ( Sass) 
5.     Javascript (Implemented Jquery and currently learning coffeescript.I have written one functionality. I am happy to implement more functionalities ,please let me know)
6.	Responsive Website (Code has been implemented for mobile and tablet)

Setup details:

1.	Download the copy of files from bit bucket and place it in theme folder
2.	Set up the WordPress environment by using given database
3.	Create a page called home and make it as front page
4.	Run task runner using ‘gulp’ command in command line NPM to see the end product

2) Plugin : Advance custom field (https://www.advancedcustomfields.com/)
I have made use ACF to make editable container such as

1.	Logo
2.	Heading of Section One
3.	Content of Section One
4.	Content of Section Two( Red Background)
5.	Recipe Image , title , and content
6.	Footer Logo

I have provided ACF.xml file, please use it to see all the above functionalities implemented.
Please let me know for any corrections or updates to be done.




