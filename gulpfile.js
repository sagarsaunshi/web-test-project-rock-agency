var gulp   	= require('gulp'),
    sass   	= require('gulp-sass'),
    concat 	= require('gulp-concat'),
    jshint = require('gulp-jshint'),
    imagemin = require('gulp-imagemin'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    browserSync = require('browser-sync').create();
   
 
gulp.task('sass', function () {
    return gulp.src('./assets/sass/**/*.scss')
    .pipe(plumber(plumberErrorHandler))
   .pipe(sass())
   .pipe(gulp.dest('./'))
    .pipe(browserSync.reload({
      stream: true
    }))
});
 
gulp.task('js', function () {
   return gulp.src('./assets/js/app/**/*.js')
    .pipe(plumber(plumberErrorHandler))
    .pipe(jshint())
    .pipe(jshint.reporter('fail'))
    .pipe(concat('script.js'))
    .pipe(gulp.dest('./assets/js'))
    .pipe(browserSync.reload({
      stream: true
    }))
});
	
gulp.task('img', function() {
  return gulp.src('./assets/images/app/*.{png,jpg,gif,svg}')
    .pipe(plumber(plumberErrorHandler))
    .pipe(imagemin({
      optimizationLevel: 7,
      progressive: true
    }))
    .pipe(gulp.dest('./assets/images'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('watch',['browserSync'], function() {
  gulp.watch('./assets/sass/**/*.scss', ['sass']);
  gulp.watch('./assets/js/app/**/*.js', ['js']);
  gulp.watch('./assets/images/app/*.{png,jpg,gif,svg}', ['img']);
});



gulp.task('default', ['sass', 'js','img','watch']);

var plumberErrorHandler = { errorHandler: notify.onError({
    title: 'Gulp',
    message: 'Error: <%= error.message %>'
  })
};

//Browser Sync
gulp.task('browserSync', function() {
  browserSync.init({
    proxy: "http://localhost/rock",
    serveStatic: ['.', './']
  })
})