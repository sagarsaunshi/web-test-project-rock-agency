<?php
define("THEME_DIR", get_template_directory_uri());
function enqueue_styles() {
     
    /** REGISTER css/screen.css **/
    wp_register_style( 'screen-style', THEME_DIR . '/style.css', array(), '1', 'all' );
    wp_enqueue_style( 'screen-style' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles' );

//ENQUEUE SCRIPTS//
function enqueue_scripts(){
    //REGISTER custom.js //
    wp_register_script( 'custom-script', THEME_DIR. '/assets/js/script.js',array('jquery'),'1.0.0', true );
      wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );

//Content Width //
if ( ! isset( $content_width ) ) {
	$content_width = 600;
}

//ENABLE FEATURED IMAGE//
add_theme_support('post-thumbnails');

//ADDING MENU SUPPORT//
function sagar_theme_setup(){
    add_theme_support('menus');
    register_nav_menu('primary','Primary Menu');
    register_nav_menu('secondary','Footer Menu');
}
add_action( 'init','sagar_theme_setup' );
?>