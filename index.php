<?php get_header(); ?>
<!--Header Section--->
<section id='header'>
    <div id='header-image'>
       <?php $image = get_field('logo');
        if( !empty($image) ): ?>
        <img id='img' src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?>
    </div>
</section>
<!--Header Section Ends--->
<!--Section One --->
<section id='section-one'>
    <div class="container">
        <h1><?php echo get_field('section_one_heading'); ?></h1>
        <p class="one">
            <?php echo get_field('section_one_content'); ?>
        </p>
        <br/>
        <p class="two">
            It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </p>
        <div class="button">
            <div class="button-one"><p>LEARN MORE</p></div>
        </div>
    </div>
</section>
<!--Section One Ends --->
<!--Section Two --->
<section id='section-two'>
    <div class="container-one">
        <?php echo get_field('section_two_content'); ?>
    </div>
</section>
<!--Section Two Ends --->
<!--Section Three --->
<section id='section-three'>
     <div class="container group">
        <h1>Get ready for glowing skin, vibrant health and renewed energy</h1>
         <div class="left-container alignleft">
            <h2>Lorem Ipsum Dolor</h2>
             <p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
         
         </div>
         <div class="right-container alignright">
             <h2>Lorem Ipsum Dolor</h2>
            <p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
         </div>
    </div>
</section>
<!--Section Three Ends --->
<!--Section Four --->
<section id='section-four' class="group">
    <div id="images">
        <div id='img1'><?php $image = get_field('section_four_image1');
        if( !empty($image) ): ?>
        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?> </div>
            <div id='img1'><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/image4.jpg" /></div>    
            <div id='img1'><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/image5.jpg" /></div>   
            <div id='img1'><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/image7.jpg" /></div>   
    </div>
</section>
<!--Section Four Ends --->
<!--Section Five --->
<section id='section-five'>
      <div class="container group">
          <h1>Lorem psum Dolor</h1>
         <div class="left-container alignleft">
            <img class='five-img' src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/image1.jpg" />
            <h2>Lorem Ipsum Dolor</h2>
             <p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
             <div class="button button-five">
             <div class="button-one"><p>FIND OUT MORE</p></div>
            </div>
         </div>
         <div class="right-container alignright">
             <img  class='five-img' src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/image2.jpg" />
             <h2>Lorem Ipsum Dolor</h2>
            <p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            <div class="button button-five">
                <div class="button-one"><p>FIND OUT MORE</p></div>
            </div>
         </div>
    </div>
</section>
<!--Section Five Ends --->
<!--Section Six --->
<section id='section-six'>
    <div class="container-six container ">
        <h1>Lorem Ipsum Dolor</h1>
        <p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div class="recipes group">
                <div class="recipe-img alignleft"><?php $image =    get_field('section_six_recipe_image');
                    if( !empty($image) ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <?php endif; ?> </div>
                    <div class="recipe-info alignright">
                        <h3><?php echo get_field('section_six_recipe_title'); ?> </h3>
                        <p><?php echo get_field('section_six_recipe_content'); ?> </p>
                        <div class="recipe-button">
                            <div class="recipe-button-one"><p>FIND OUT MORE</p></div>
                        </div>
                    </div>
            </div>
        <div class="recipes group">
             <div class="recipe-img alignleft"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/image8.jpg" /></div>
                <div class="recipe-info  alignright">
                    <h3>Lorem Ipsum Dolor </h3>
                    <p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <div class="recipe-button">
                            <div class="recipe-button-one"><p>FIND OUT MORE</p></div>
                        </div>
                </div>
        </div>
        <div class="recipes group">
             <div class="recipe-img alignleft"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/image3.jpg" /></div>
                <div class="recipe-info  alignright">
                    <h3>Lorem Ipsum Dolor </h3>
                    <p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <div class="recipe-button">
                            <div class="recipe-button-one"><p>FIND OUT MORE</p></div>
                        </div>
                </div>
        </div>
    </div>
</section>
<!--Section Six Ends --->
<!--Section Seven --->
<section id='section-seven'>
    <div class="container-seven">
        <div id='footer-image'> <?php $image = get_field('logo');
            if( !empty($image) ): ?>
            <img id='img-footer' src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <?php endif; ?></div>
            <div id='seven-info'>
                <h1>Lorem Ipsum Dolor</h1>
                    <div class="button">
                        <div class="button-one"><p>FIND OUT MORE</p></div>
                    </div>
            </div>    
    </div>
</section>
<!--Section Seven Ends --->
<div class="scroll-up">
<p>&#x039B</p>
</div>
<?php get_footer(); ?>